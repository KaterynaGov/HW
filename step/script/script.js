const tabs = ()=>{
    const currentTab = (event)=>{

        const tabButtons = document.querySelectorAll(`[data-tab]`);
        tabButtons.forEach(button =>{
            if (button.classList.contains(`active`)) {
                button.classList.remove(`active`);
            }
        })

        if(!event.target.classList.contains(`active`)){
			event.target.classList.add(`active`);
		}
        const contentClass = event.target.getAttribute('data-tab')
        
        let content = document.querySelectorAll('.tabs__item');

        content.forEach(item=>{
            if (item.classList.contains(`active`)) {
                item.classList.remove('active')                
            }

        })
        if(!document.querySelector(`.tabs__content .${contentClass}`).classList.contains(`active`)){
            document.querySelector(`.tabs__content .${contentClass}`).classList.add(`active`)
        }
    }

    let tabs = document.querySelector('.tabs__header');

    const touchButton = (event) =>{
        if (event.target.nodeName === `BUTTON`) {
            currentTab(event)
        }
    }

    tabs.addEventListener('click',touchButton)

}
tabs()

const swiper = ()=>{
    const test = new Swiper(".thumbs-slider", {
        spaceBetween: 25,
        slidesPerView: 'auto',
        freeMode: true,
        watchSlidesProgress: true,
    });
    
    const swiperMain = new Swiper(".slider", {
        spaceBetween: 1,
        navigation: {
            nextEl: ".swiper-button-next ",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: test,
        },
    });
}
swiper()


const filter = () =>{
    const galleryMenu = document.querySelector('.work__header');

    const button = document.querySelector('.work__button');

    const galleryContent  = document.querySelector('.gallery-content');

    galleryMenu.addEventListener('click', ()=>{
        if(event.target.nodeName === 'BUTTON'){
            console.log(event.target);

            let dataMenu = null;

			dataMenu = event.target.getAttribute('data-menu');
        
            const dataGallery = document.querySelectorAll('.works__item'); 

            if(event.target.nodeName === 'BUTTON'){
                dataMenu = event.target.getAttribute('data-menu')
            }

            let menuItems = document.querySelectorAll('[data-menu]')

            menuItems.forEach(item=>{
                if (item.classList.contains('active')) {
                    item.classList.remove('active');
                }
            })

            if (!event.target.classList.contains('active')) {
                event.target.classList.add('active');
            }

            dataGallery.forEach((item) =>{
                if(dataMenu !== item.getAttribute('data-gallery') &&  dataMenu !== 'all'){
                    item.style.display = 'none';
                } 
                else {
                    item.style.display = 'block'				
                }
            }) 
		}

        
    });

    const  additionalPictures = [
        {
            data:'graphic_design',
            src:'./img/work/graphic design/graphic-design4.jpg',
            text:'Graphic Design',
        },{
            data:'web_design',
            src:'./img/work/web design/web-design4.jpg',
            text:'Web Design',
        },{
            data:'landing_page',
            src:'./img/work/landing page/landing-page4.jpg',
            text:'Landing Page',
        },{
            data:'wordpress',
            src:'./img/work/wordpress/wordpress4.jpg',
            text:'Wordpress',
        },{
            data:'graphic_design',
            src:'./img/work/graphic design/graphic-design5.jpg',
            text:'Graphic Design',
        },{
            data:'web_design',
            src:'./img/work/web design/web-design5.jpg',
            text:'Web Design',
        },{
            data:'landing_page',
            src:'./img/work/landing page/landing-page5.jpg',
            text:'Landing Page',
        },{
            data:'wordpress',
            src:'./img/work/wordpress/wordpress5.jpg',
            text:'Wordpress',
        },{
            data:'graphic_design',
            src:'./img/work/graphic design/graphic-design6.jpg',
            text:'Graphic Design',
        },{
            data:'web_design',
            src:'./img/work/web design/web-design6.jpg',
            text:'Web Design',
        },{
            data:'landing_page',
            src:'./img/work/landing page/landing-page6.jpg',
            text:'Landing Page',
        },{
            data:'wordpress',
            src:'./img/work/wordpress/wordpress6.jpg',
            text:'Wordpress',
        }
        
    ]

    console.log(additionalPictures);

    button.addEventListener('click', ()=>{
        console.log(event.target);
        additionalPictures.forEach((image, id)=>{
            console.log(image.data);
            galleryContent.insertAdjacentHTML("beforeend", `
            <div class="works__item" data-gallery="${image.data}">
                <div class="works__item_hover">
                    <div class="hover__buttons">
                        <div class="hover__button"></div>
                        <div class="hover__button"></div>
                    </div>
                    <p class="hover__title">creative design</p>
                    <p class="hover__subtitle">${image.text}</p>
                </div>
                <img src="${image.src}" alt="img" class="work__flex-box_img${id+13}">
            </div>`);
        })
        button.remove()
    })
}
filter()
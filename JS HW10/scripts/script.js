// ## Задание

// Реализовать переключение вкладок (табы) на чистом Javascript.

// #### Технические требования:
// - В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки. 
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// - Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.


// Убрать класс active со случайной кнопки
// дать старой информации display:none
// Добавить класс active выбрано кнопке
// узнать порядковый номер кнопки
// дать новой информаци display:inline

const tabs = document.querySelector(`.tabs`);
const tabsContent = document.querySelectorAll(`.tabs-content>li`)

const hideТext =(inputIndex)=>{
    tabsContent.forEach((elem,index) =>{
        if (inputIndex === index) {
            elem.style.display = 'inline'
        }else{
            elem.style.display = 'none'
        }
    })
}
const activateButton = ()=>{
    arrTabs = []
    for (const button of tabs.children) {
        arrTabs.push(button)        
    }
    let elemIndex = null;
    arrTabs.forEach((elem,index) => {
        if (event.target === elem) {
            elem.classList.add(`active`);
            elemIndex = index;
        }else{
            elem.classList.remove(`active`)
        }        
    });
    hideТext(elemIndex)
}

tabs.addEventListener('click', activateButton)


const hoverButton = () =>{
    for (const button of tabs.children) {
        if (event.target === button) {
            button.classList.add(`border_hover`);
        }else{
            button.classList.remove(`border_hover`)
        }               
    }
}

tabs.addEventListener('mouseover', hoverButton ,/*{once:true}*/)

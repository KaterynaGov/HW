// ## Задание

// Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - Взять любое готовое домашнее задание по HTML/CSS.
// - Добавить на макете кнопку "Сменить тему".
// - При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// - Выбранная тема должна сохраняться и после перезагрузки страницы

// #### Литература:
// - [LocalStorage на пальцах](https://tproger.ru/articles/localstorage/)

const nightTheme = () =>{
    const body = document.querySelector(`body`);
    body.style.backgroundColor = '#222F3A';

    const themeIconDay = document.querySelector('div[class=bg_sun]');
    themeIconDay.style.display = 'block'
    const themeIconNight = document.querySelector('div[class=bg_moon]');
    themeIconNight.style.display = 'none'

    const navMenu = document.querySelector('.header-nav-list');
    navMenu.style.backgroundColor = '#222F3A'


    const asaide  = document.querySelector('asaide');
    asaide.style.borderColor = 'white';

    const asaideItems  = document.querySelectorAll('.asaide-nav-link');
    asaideItems.forEach(item =>{
        item.style.color = 'white';
    })

    const infoHero = document.querySelector('.info-hero');
    infoHero.style.borderColor = 'white';
 
    const pargraphs = document.querySelectorAll('.info p');
    pargraphs.forEach(pargraph =>{
        pargraph.style.color = '#fff'
    })

    document.querySelector('.footer').style.backgroundColor = '#b0b4b8';

    localStorage.setItem('theme','night')
}

const dayTheme = () =>{
    const body = document.querySelector(`body`);
    body.style.backgroundColor = '#fff';

    const themeIconDay = document.querySelector('div[class=bg_sun]');
    themeIconDay.style.display = 'none';
    const themeIconNight = document.querySelector('div[class=bg_moon]');
    themeIconNight.style.display = 'inline-block';

    const navMenu = document.querySelector('.header-nav-list');
    navMenu.style.backgroundColor = '#222F3A'


    const asaide  = document.querySelector('asaide');
    asaide.style.borderColor = '#000';

    const asaideItems  = document.querySelectorAll('.asaide-nav-link');
    asaideItems.forEach(item =>{
        item.style.color = '#485863';
    })

    const infoHero = document.querySelector('.info-hero');
    infoHero.style.borderColor = '#000';
 
    const pargraphs = document.querySelectorAll('.info p');
    pargraphs.forEach(pargraph =>{
        pargraph.style.color = '#000'
    })

    document.querySelector('.footer').style.backgroundColor = '#63696E'

    localStorage.setItem('theme','day')
}

window.addEventListener('load', ()=>{
    if (localStorage.getItem('theme') == `day`) {
        dayTheme();
    } else if(localStorage.getItem('theme') == `night`){
        nightTheme();
    }
})

document.addEventListener('click',(event)=>{
    const themeIconNight = document.querySelector('.bi-moon-stars');
    const themeIconDay = document.querySelector('.bi-brightness-high');
    if (event.target.nodeName === `svg`) {
        if (event.target === themeIconNight) {
            nightTheme()
        }else if(event.target === themeIconDay) {
            dayTheme()
        }
    }
})



// dayTheme()

// nightTheme()



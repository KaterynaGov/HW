// ## Задание

// Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
//   1.  При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
//   2.  Создать метод `getAge()` который будет возвращать сколько пользователю лет.
//   3.  Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
// - Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.




const createNewUser = () =>{
    const newUser = Object.create({},{
        firstName:{
            value:prompt(`Введите имя.`),
            
        },
        lastName:{
            value:prompt(`Введите фамилию.`),
        },
        birthday:{
            value:`${Math.trunc(prompt('Введите месяц рождения.'))},${Math.trunc(prompt('Введите день рождения.'))},${Math.trunc(prompt('Введите год рождения.'))}`

        },

        // Вот еще вариант ключа  "birthday", только с проверками на то, какая информация вводится, почему-то "birthday" не видит метод "birthdayDate()".

        // birthday:{
        //     value: new Date (this.birthdayDate()),
        // },
        // birthdayDate:{
        //     value:function birthdayDate(day, month, year){
        //         year =  Math.trunc(prompt('Введите год рождения.'));
        //         while (!Boolean(Number(year)) || year > 2010 || year < 1960 ) {
        //             year = Math.trunc(prompt('Введите год рождения.'));
        //         }
        //         month = Math.trunc(prompt('Введите месяц рождения.'));
        //         while (!Boolean(Number(month)) || month > 12 || month < 1 ) {
        //             month = Math.trunc(prompt('Введите месяц рождения.'));
        //         }
        //         day = prompt('Введите день рождения.');
        //         while (!Boolean(Number(day)) || day > 31 || day < 1 ) {
        //             day = Math.trunc(prompt('Введите день рождения.'));
        //         }
        //         value =`${month}, ${day}, ${year}`;
        //         
        //         return value;
        //     }
        // },
        getPassword:{
            value: function(){
                return this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthday.substr(-4, 4);
            }
        },
        getAge:{
            value:function(){
                return  Math.trunc(Math.abs(new Date().getTime() - new Date(this.birthday).getTime())/(1000 * 3600 * 24 * 365));
            }
        },
    });
    return newUser; 
}

// console.log(createNewUser());
let user = createNewUser();
console.log(user);
console.log(user.getPassword());
console.log(user.getAge());
// ## Задание

// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:

// - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.



const createNewUser = () =>{
    const newUser = Object.create({},{
        'firstName':{
            value: prompt(`Введите имя.`), 
            configurable: true,
            writable: false,
        },
        lastName:{
            value:prompt(`Введите фамилию.`),
            configurable: true,
            writable: false,
        },
        getLogin:{
            value:function (value){
                return (this.firstName[0]+this.lastName).toLowerCase();
            },
        },
        setFirstName: {
            value: function (value) {
                Object.defineProperty(this, 'firstName', {value: value,});
            }
        },
        setLastName:{
            value: function(value){
                Object.defineProperty(this,lastName,{value: value,});
            }
        },
    });

    return newUser; 
}

let user = createNewUser();
console.log(user.getLogin());
user.setFirstName(`not${user.firstName}`);
console.log(user.firstName);




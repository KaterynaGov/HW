// ## Задание

// Создать объект "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - Создать пустой объект `student`, с полями `name` и `lastName`.
// - Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
// - В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента `tabel`.
// - Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение `Студент переведен на следующий курс`.
// - Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение `Студенту назначена стипендия`.

// #### Литература:
// - [Объекты как ассоциативные массивы](https://learn.javascript.ru/object)
// - [Перебор свойств объектов](https://learn.javascript.ru/object-for-in)


const student = {
    name:name,
    "lastName":"",
    tabel:{

    },
}

student.name = prompt('Введите имя студента.');
student.lastName = prompt('Введите фамилию студента.');
let subject;
let grade;

while(true){
    subject = prompt("Веедите название предмета.");
    if(!Boolean(subject)){
        break;
    }
    grade = +prompt("Введите оценку от 1 до 10.");
    while( isNaN(grade) || grade  > 10 || grade < 1){
        grade = +prompt("Введите оценку от 1 до 10.")
        if (grade === 0){
            break;
        }
    }
    if(!Boolean(grade)){
        break;
    }
    student.tabel[subject] = grade;
}
let studentBadRating = null;
let numberOfSubjects = null;
let sumOfGrades = null;

for (const key in student.tabel) {
    if (student.tabel[key] < 4){
        studentBadRating++;
    }
    numberOfSubjects++;
    sumOfGrades +=student.tabel[key];
}
if(studentBadRating === null){
    alert('Студент переведен на следующий курс')
}
if (sumOfGrades/numberOfSubjects > 7){
    alert('Студенту назначена стипендия')
}

console.log(student);
console.log(student.tabel);
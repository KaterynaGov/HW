// ## Задание

// Реализовать функцию фильтра массива по указанному типу данных. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Написать функцию `filterBy()`, которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// - Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].

// #### Литература:

// - [Массивы с числовыми индексами](http://learn.javascript.ru/array)
// - [Массив: перебирающие методы](http://learn.javascript.ru/array-iteration)
// - [Шесть типов данных, typeof](https://learn.javascript.ru/types-intro)

const filterBy = function (array, type){
    const newArray =[];
    for(let i=0; i<array.length;i++){
        if(typeof array[i] !== type){
        newArray.push(array[i]);
        };
    };
    return newArray;
};

let datatype = prompt('Введите тип данных').toLowerCase();
while(datatype !== 'string' && datatype !== 'number' && datatype !== 'boolean' && datatype !== 'object' && datatype !== 'undefined'){
    datatype = prompt('Введите тип данных').toLowerCase();
}

console.log(filterBy([[1], Infinity, null, undefined, true, 'string', false, 10], datatype))


// #### Технические требования:

// - Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// - Каждый из элементов массива вывести на страницу в виде пункта списка;

// Примеры массивов, которые можно выводить на экран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можно взять любой другой массив.

// #### Необязательные задания продвинутой сложности:

// 1. Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив, выводить его как вложенный список.
//    Пример такого массива:

//    ```javascript
//    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//    ```

//    > Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.

// 2. Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.

// #### Литература:

// - [Поиск DOM элементов](https://learn.javascript.ru/searching-elements-dom)
// - [Добавление и удаление узлов](https://learn.javascript.ru/modifying-document)
// - [Шаблонные строки](http://learn.javascript.ru/es-string)
// - [Array.prototype.map()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/map)
// - [setTimeout, setInterval](https://learn.javascript.ru/settimeout-setinterval)


// const parent = document.body

const list = (array, parent = document.body) => {
    const newList = document.createElement('ul');
    newList.className = `mainList`;
    parent.prepend(newList);

    array.forEach(elem => {
        if (Array.isArray(elem)){
            const newSubList = document.createElement('ul');
            newSubList.className = `subList`;
            newList.append(newSubList);
            elem.forEach(subElem =>{
                let subLi = document.createElement(`li`);
                subLi.className = `subListItem`;
                subLi.innerHTML = `<p>${subElem}</p>`;
                newSubList.append(subLi);
            })
        }else{
        let li = document.createElement(`li`);
        li.className = `listItem`;
        li.innerHTML = `<p>${elem}</p>`;
        newList.append(li);
        }
    });


    let count = parseInt(prompt('After how many seconds to clear the page?', "3"));
    
    while (count === null || Number.isNaN(count) || count < 1) {
        count = parseInt(prompt('After how many seconds to clear the page?', "3"));  
    }
    const timer =  document.createElement(`div`);
    timer.style.color = `red`;
    timer.style.fontSize = `150%`;

    timer.innerHTML = `<p>Page will be cleared in : ${count} seconds.</p>`;

    let refreshIntervalId = setInterval( request=()=> {
        count--
        timer.innerHTML = `<p>Page will be cleared in : ${count} seconds.</p>`;
        if (count === 0){
            document.querySelector(`ul`).remove();
            document.querySelector(`div`).remove();
            clearInterval(refreshIntervalId);
        }
    }, 1000)
    console.log(newList);
    parent.insertBefore(timer, parent.querySelector(`ul`));  
}

const theArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]

list(theArray)

// list(["1", "2", "3", "sea", "user", 23]) 

// setTimeout(clean = () => document.querySelector(`.mainList`).remove(), 5000)

// ## Задание

// Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - В файле `index.html` лежит разметка для двух полей ввода пароля.
// - По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
// - Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// - Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// - По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// - Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
// - Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые значения`
// - После нажатия на кнопку страница не должна перезагружаться
// - Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.


const inputs = document.querySelectorAll(`input`)
const eyesIcons = document.querySelectorAll(`.fas`)

const showPasword = (event) => {
    if (event.target.classList.contains(`fa-eye`)) {
        eyesIcons.forEach((elem) =>{
            elem.classList.remove(`fa-eye`);
            elem.classList.add(`fa-eye-slash`);
        })
        inputs.forEach((elem) =>{
        elem.type = 'text'
        })
    }else if (event.target.classList.contains(`fa-eye-slash`)) {
        eyesIcons.forEach((elem) =>{
            elem.classList.remove(`fa-eye-slash`);
            elem.classList.add(`fa-eye`);
        })
        inputs.forEach((elem) =>{
            elem.type = 'password'
            })
    }  
}

document.body.addEventListener('mouseup', showPasword)

const button = document.querySelector('.btn')

const inputValid = ()=>{
    if (inputs[0].value === inputs[1].value && inputs[0].value !== '') {
        return true;
    } else{
        return false;
    }
}
const cleanMassage = () =>{
    if (button.previousElementSibling.innerHTML === `Hужно ввести одинаковые значения`) {
        button.previousElementSibling.remove();
    }
}
const cleanInputs = () =>{
    for (const text of inputs) {
        text.value = null ;       
    }
    
}

button.addEventListener('mouseup',()=>{
    if (inputValid()) {
        alert('You are welcome')
        cleanInputs();
        cleanMassage();
    }else{
        cleanMassage();
        button.insertAdjacentHTML('beforebegin', '<p>Hужно ввести одинаковые значения</p>')
        button.previousElementSibling.style.color ='red';
    }
    
})

// - Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// - Если возраст меньше 18 лет - показать на экране сообщение: `You are not allowed to visit this website`.
// - Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: `Are you sure you want to continue?` и кнопками `Ok`, `Cancel`. Если пользователь нажал `Ok`, показать на экране сообщение: `Welcome, ` + имя пользователя. Если пользователь нажал `Cancel`, показать на экране сообщение: `You are not allowed to visit this website`.
// - Если возраст больше 22 лет - показать на экране сообщение: `Welcome, ` + имя пользователя.
// - Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
// - После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

let first_name = prompt("Введите имя пользователя.");

while(first_name === null || first_name === ""){
    first_name = prompt("Введите имя пользователя.");
}

let age = +prompt("Ведите возраст пользователя.");

while(isNaN(age) || age <= 0 || age === null || age === ""){
    age = +prompt("Ведите возраст пользователя.");
}

let question;

if(age < 18){
    alert('Вам запрещено посещать этот сайт.');
}
else if(age >= 18 && age <= 22){
    question = confirm('Вы уверены что хотите продолжить?');
    if (question == true){
        alert(`Welcome, ${first_name}.`);
    }
    else{
        alert('Вам запрещено посещать этот сайт.');
    }
}
else{
    alert(`Welcome, ${first_name}.`)
}

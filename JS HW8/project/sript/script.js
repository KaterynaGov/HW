// ## Задания

// Код для заданий лежит в папке project.

// 1) Найти все параграфы на странице и установить цвет фона #ff0000

// 2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.

// 3) Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>

// 4) Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item. 

// 5) Найти все элементы с классом section-title. Удалить этот класс у элементов.  


// #### Литература:

// - [Поиск DOM-элементов](https://learn.javascript.ru/searching-elements-dom)


// 1) Найти все параграфы на странице и установить цвет фона #ff0000


const paragraph = document.getElementsByTagName('p');
console.log(paragraph);

for (const elem of paragraph) {
    console.log(elem);  
    elem.style.backgroundColor = '#ff0000';
}




// 2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод. 

const optionsList = document.querySelector('#optionsList')
console.log(optionsList);
console.log(optionsList.parentNode);
if (optionsList.hasChildNodes() === true) {
    for (const child of optionsList.childNodes) {
        let nodeType = null;
        if (child.nodeType === 1) {
            nodeType = 'элемент';     
        } else if (child.nodeType === 3){
            nodeType = 'текст';
        }
        console.log(`Название ноды: ${child.nodeName}, тип ноды: ${nodeType}`);
    }
}



// 3) Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>


const testParagraph = document.getElementById('testParagraph');
console.log(testParagraph);
testParagraph.innerHTML = '<p>This is a paragraph</p>';
console.log(testParagraph);



// 4) Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item. 

// const headerList = document.getElementsByClassName('main-header');
// console.log(headerList);


const headerList = document.querySelectorAll('.main-header li');
console.log(headerList);
headerList.forEach(element => {
    element.classList.add('nav-item')
});
console.log(headerList);




// 5) Найти все элементы с классом section-title. Удалить этот класс у элементов.


const sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle); // такого класса, тега или айди нету в документе

// но если бы был, то :

sectionTitle.forEach(element => {
	element.classList.remove("section-title");
});
console.log(sectionTitle);
